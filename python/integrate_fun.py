# Integration for some functions (potentials mostly)

import numpy as np
from sympy import integrate, symbols

def main():
  r, rp, rs = symbols('r rp rs')
  fun = rp**2/(rp**2+rs**2)
  fun = rp**2

  integ = integrate(fun, (rp, 0, r))
  print(integ)

if __name__ == '__main__':
  main()
