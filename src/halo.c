#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_sf_gamma.h> /* for gamma and incomplete gamma functions */

#include "allvars.h"
#include "proto.h"


/* this file contains auxiliary routines for the description of the halo,
 * here modeled as a Hernquist sphere
 */

/* this function returns a new random coordinate for the halo */
void halo_get_fresh_coordinate(double *pos)
{
  double r;
  int out; // flag to exit while loop

  /* Adding a cut-off radius - Alex 04/2021 */
  double rcut = solve_secant(All.DistHalo, 0.95, All.Halo_A, 
                             All.Halo_A*0.95, All.Halo_A*0.95 + 1, All.GammaEin, 0.00005);

  /* radius rings to assign probability - Alex 04/2021 */
  int nrings = 10;
  double init = rcut, fin = All.Rmax; // beginning and end of rings
  
  /* create rings (linear) - Alex 04/2021 */
  int cr; // current ring
  double rad[nrings]; // array with all the rings
  for (cr = 0; cr < nrings; cr++)
    rad[cr] = init + (fin-init) / (nrings-1) * cr;
  
  /* create array with probability for each ring  - Alex 04/2021 */
  double probr[nrings-1], p, pinf;
  for (cr = 0, p = -1.0, pinf = -0.1; cr < nrings-1; cr++)
    probr[cr] = exp(p + (-p+pinf) / (nrings-2) * cr);
  
  /* for Hernquist */
  if (All.DistHalo == 0 || All.DistHalo == 4)
    rcut = All.Rmax;

  do
    {
      double q = gsl_rng_uniform(random_generator); //Commented - Alex 04/2021

      double x0 = All.Halo_A * q; // initial guess for r - Alex 04/2020
      double x1 = x0 + 1; // second initial guess for r - Alex 04/2020
      double error = 0.00005; // error for secant method - Alex 04/2020

      /* Commented - Alex 04/2020
      if(q > 0)
        r = All.Halo_A * (q + sqrt(q)) / (1 - q);
      else
        r = 0;
      */

      /* Alex 04/2020 */
      if (q > 0)
        r = solve_secant(All.DistHalo, q, All.Halo_A, x0, x1, All.GammaEin, error);
      else
        r = 0;
      /* Alex 04/2020 */

      double phi = gsl_rng_uniform(random_generator) * M_PI * 2;
      double theta = acos(gsl_rng_uniform(random_generator) * 2 - 1);

      pos[0] = r * sin(theta) * cos(phi);
      pos[1] = r * sin(theta) * sin(phi);
      pos[2] = r * cos(theta) / All.HaloStretch;

      r = sqrt(pos[0]*pos[0] + pos[1]*pos[1] + pos[2]*pos[2]);

      /* Applying radius cut-off - Alex 04/2021 */
      if (All.DistHalo != 0 && All.DistHalo != 4) {
        out = 0;
        if (r >= rcut && r <= All.Rmax) {
          p = gsl_rng_uniform(random_generator);
          for (cr = 0; cr < nrings-1; cr++)
            if (r >= rad[cr] && r <= rad[cr+1])
              if (p > probr[cr]) {
                out = 1;
                break;
              }
          if (out)
            break;
          else
            continue;
        }   
      }
    }
  //while(r > All.Rmax); Commented - Alex 04/2021
    while(r > rcut);
}

/* modified - Alex 04/2020
 * Formulas for Einasto profile are described in Cardone, Piedipalumbo & Tortora (2005)
 * and Navarro et al. (2004).
 */
double halo_get_density(double *pos) {
	int htype = All.DistHalo;
  double r = sqrt(pos[0] * pos[0] + pos[1] * pos[1] + All.HaloStretch * All.HaloStretch * pos[2] * pos[2]);
  double rho;

  double x = r / All.Halo_A; /* factor for scale radius of Einasto profile */
  double rho_min_2 = All.HaloStretch * All.Halo_Mass / (16 * M_PI) / pow(All.Halo_A, 3);

  switch (htype) {
    case 0: // Hernquist
      rho = All.HaloStretch * All.Halo_Mass / (2 * M_PI) * All.Halo_A /
                   (r + 1.0e-6 * All.Halo_A) / pow(r + All.Halo_A, 3);
      break;
    case 1: // NFW
      rho = All.HaloStretch * All.Halo_Mass / (4 * M_PI) / (log(1+All.Halo_C) - All.Halo_C/(1+All.Halo_C)) / (r + 1.0e-6 * All.Halo_A) /
                   pow(r + All.Halo_A, 2);
      break;
    case 2: // Pseudo-isothermal
      rho = All.HaloStretch * All.Halo_Mass / (4 * M_PI) / (All.Halo_C - atan(All.Halo_C)) / All.Halo_A /
            (r * r + All.Halo_A * All.Halo_A);
      break;
    case 3: // Burkert
      /*rho = All.HaloStretch * All.Halo_Mass / (2 * M_PI) / (log(1+All.Halo_C) + 0.5*log(1+All.Halo_C*All.Halo_C) - atan(All.Halo_C)) /
            (r + All.Halo_A) / (r * r + All.Halo_A * All.Halo_A);*/
      m0 = halo_get_mass_inside_radius(All.Halo_A);
      rho = 2*m0/M_PI /
            (r + All.Halo_A) / (r * r + All.Halo_A * All.Halo_A);
      break;
    case 4: // Einasto
      rho = rho_min_2 * exp(-(2/All.GammaEin) * (pow(x, All.GammaEin) - 1));
      break;
    default:
      terminate("\nHalo distribution %d does not exist.", htype);
      break;
  }

  if ( fabs(rho) <  MIN_DENSITY) rho = 0;

  return rho;
}
/* modified - Alex 04/2020 */


/* Note that the other functions below will only be called in a meaningfull for a spherical system */
/* modified - Alex 04/2020 */
double halo_get_mass_inside_radius(double r)
{
  int htype = All.DistHalo;
  double mass;

  double x = r / All.Halo_A; /* factor for scale radius of Einasto profile */

  switch (htype) {
    case 0: // Hernquist
      mass = All.Halo_Mass * pow(r / (r + All.Halo_A), 2);
      break;
    case 1: // NFW
      mass = All.Halo_Mass / (log(1+All.Halo_C) - All.Halo_C/(1+All.Halo_C)) * (log(1 + r / All.Halo_A) - r / (r + All.Halo_A));
      break;
    case 2: // Pseudo-isothermal
      mass = All.Halo_Mass / (All.Halo_C - atan(All.Halo_C)) * (r / All.Halo_A - atan(r / All.Halo_A));
      break;
    case 3: // Burkert
      mass = All.Halo_Mass / (log(1+All.Halo_C) + 0.5*log(1+All.Halo_C*All.Halo_C) - atan(All.Halo_C)) * 
             (log(1 + r / All.Halo_A) + 0.5 * log(1 + (r * r) / (All.Halo_A * All.Halo_A)) - atan(r / All.Halo_A));
      break;
    case 4: // Einasto
      mass = All.Halo_Mass * (gsl_sf_gamma(3/All.GammaEin) -
                     gsl_sf_gamma_inc(3/All.GammaEin, 2 * pow(x, All.GammaEin) / All.GammaEin))
                     / gsl_sf_gamma(3/All.GammaEin);
      break;
    default:
      terminate("\nHalo distribution %d does not exist.", htype);
      break;
  }

  return mass;
}
/* modified - Alex 04/2020 */

double halo_get_potential(double *pos)
{
  double r = sqrt(pos[0] * pos[0] + pos[1] * pos[1] + pos[2] * pos[2]);
  return halo_get_potential_from_radius(r);
}

/* modified - Alex 04/2020 */
double halo_get_potential_from_radius(double r)
{
  int htype = All.DistHalo;
  double phi;

  double x = (r + 1.0e-6 * All.Halo_A) / All.Halo_A; /* factor for scale radius of Einasto profile */
  double phi_ein = -All.G * All.Halo_Mass / All.Halo_A; /* initial potential of Einasto */

  switch (htype) {
    case 0: // Herquist
      phi = -All.G * All.Halo_Mass / (r + All.Halo_A);
      break;
    case 1: // NFW
      phi = -All.G * All.Halo_Mass / (log(1+All.Halo_C) - All.Halo_C/(1+All.Halo_C)) * log(1 + r / All.Halo_A) / (r + 1.0e-6 * All.Halo_A);
      break;
    case 2: // Pseudo-isothermal
      phi = All.G * All.Halo_Mass / (All.Halo_C - atan(All.Halo_C)) * (-atan(r / All.Halo_A) / (r + 1.0e-6 * All.Halo_A)
            - log((r * r) / (All.Halo_A * All.Halo_A)) / 2 / All.Halo_A + log(r + 1.0e-6 * All.Halo_A) / All.Halo_A);
      break;
    case 3: // Burkert
      phi = All.G * All.Halo_Mass / (log(1+All.Halo_C) + 0.5*log(1+All.Halo_C*All.Halo_C) - atan(All.Halo_C)) *
            (M_PI / (2 * All.Halo_A) - 2 * (1 / All.Halo_A + 1 / (r + 1.0e-6 * All.Halo_A)) * atan(r / All.Halo_A)
            + 2 * (1 / All.Halo_A + 1 / (r + 1.0e-6 * All.Halo_A)) * log(1 + r / All.Halo_A)
            - (1 / All.Halo_A - 1 / (r + 1.0e-6 * All.Halo_A)) * log(1 + (r * r) / (All.Halo_A * All.Halo_A)));
      break;
    case 4: // Einasto
      phi = phi_ein * (gsl_sf_gamma(3/All.GammaEin)
                       - gsl_sf_gamma_inc(3/All.GammaEin, 2 * pow(x, All.GammaEin) / All.GammaEin))
                       / gsl_sf_gamma(3/All.GammaEin) / x +
            phi_ein * pow(2/All.GammaEin, 1/All.GammaEin)
                    * gsl_sf_gamma_inc(2/All.GammaEin, 2 * pow(x, All.GammaEin) / All.GammaEin)
                       / gsl_sf_gamma(3/All.GammaEin);
      break;
    default:
      terminate("\nHalo distribution %d does not exist.", htype);
      break;
  }

  return phi;
}
/* modified - Alex 04/2020 */

/* returns the acceleration at coordinate pos[] */
/* modified - Alex 04/2020 */
void halo_get_acceleration(double *pos, double *acc)
{
  int htype = All.DistHalo;
  double r = sqrt(pos[0] * pos[0] + pos[1] * pos[1] + pos[2] * pos[2]);
  double fac;

  double x = (r + 1.0e-6 * All.Halo_A) / All.Halo_A; /* factor for scale radius of Einasto profile */
  double aein = All.G * All.Halo_Mass * (gsl_sf_gamma(3 / All.GammaEin) - gsl_sf_gamma_inc(3 / All.GammaEin, 2 / All.GammaEin))
                / gsl_sf_gamma(3 / All.GammaEin) / pow(All.Halo_A, 3); /* acceleration of Einasto */

  switch (htype) {
    case 0: // Hernquist
      fac = All.G * All.Halo_Mass / ((r + 1.0e-6 * All.Halo_A) * (r + All.Halo_A) * (r + All.Halo_A));
      break;
    case 1: // NFW
      fac = All.G * All.Halo_Mass / (log(1+All.Halo_C) - All.Halo_C/(1+All.Halo_C)) * (log(1 + r / All.Halo_A) - r / (r + All.Halo_A)) /
            pow(r + 1.0e-6 * All.Halo_A, 3);
      break;
    case 2: // Pseudo-isothermal
      fac = All.G * halo_get_mass_inside_radius(r) / pow(r + 1.0e-6 * All.Halo_A, 3);
      break;
    case 3: // Burkert
      fac = All.G * halo_get_mass_inside_radius(r) / pow(r + 1.0e-6 * All.Halo_A, 3);
      break;
    case 4: // Einasto
      fac = aein * (gsl_sf_gamma(3 / All.GammaEin) - gsl_sf_gamma_inc(3 / All.GammaEin, 2 * pow(x, All.GammaEin) / All.GammaEin))
            / (gsl_sf_gamma(3 / All.GammaEin) - gsl_sf_gamma_inc(3 / All.GammaEin, 2 / All.GammaEin)) / pow(x, 3);
      break;
    default:
      terminate("\nHalo distribution %d does not exist.", htype);
      break;
  }

  acc[0] = -fac * pos[0];
  acc[1] = -fac * pos[1];
  acc[2] = -fac * pos[2];
}
/* modified - Alex 04/2020 */

/* modified - Alex 04/2020 */
double halo_get_escape_speed(double *pos)
{
  double r = sqrt(pos[0] * pos[0] + pos[1] * pos[1] + pos[2] * pos[2]);
  // double phi = -All.G * All.Halo_Mass / (r + All.Halo_A); Commented by Alex 04/2020
  double phi = halo_get_potential_from_radius(r); /* Alex 04/2020 */
  double vesc = sqrt(-2.0 * phi);

  return vesc;
}
/* modified - Alex 04/2020 */

double halo_get_sigma2(double *pos) {

	long double r = sqrt(pos[0]*pos[0] + pos[1]*pos[1] + pos[2]*pos[2]);

	long double m = All.Halo_Mass;
	long double r0 = All.Halo_A;
	long double r_over_r0 = r/r0;

	long double _sigma2 =
	(long double)(All.G*m)/(12.0*r0)*
	fabs( 12*r*powl(r+r0,3)/powl(r0,4)*logl((r+r0)/r)
			-
			r/(r+r0)*(25 + r_over_r0*(52 + 42*r_over_r0 + 12*(r_over_r0*r_over_r0) ) )
		 );

	// precision big rip so let it be like this for a while
	if (65000<r)
		_sigma2 = pow(halo_get_escape_speed(pos)/3.14, 2);
		//_sigma2 = SQR(vesc(_r)/3.14);


	return _sigma2;

}

/*E to q conversion*/
double halo_E_to_q(double E)
{
  return sqrt(-E * All.Halo_A / (All.G * All.Halo_Mass));
}

/*Hernquist density of states (as a function of q)*/
double halo_g_q(double q)
{
  double pre =
    2 * sqrt(2) * M_PI * M_PI * All.Halo_A * All.Halo_A * All.Halo_A * sqrt(All.G * All.Halo_Mass /
									    All.Halo_A);

  return pre * (3 * (8 * q * q * q * q - 4 * q * q + 1) * acos(q) -
		q * sqrt(1 - q * q) * (4 * q * q - 1) * (2 * q * q + 3)) / (3 * q * q * q * q * q);
}

/*Hernquist distribution function (as a function of q)*/
double halo_f_q(double q)
{
  double pre =
    (All.Halo_Mass / (All.Halo_A * All.Halo_A * All.Halo_A)) / (4 * M_PI * M_PI * M_PI *
								pow(2 * All.G * All.Halo_Mass / All.Halo_A,
								    1.5));

  return pre * (3 * asin(q) +
		q * sqrt(1 - q * q) * (1 - 2 * q * q) * (8 * q * q * q * q - 8 * q * q - 3)) / pow(1 - q * q,
												   2.5);
}

/*Hernquist distribution function (as a function of radius and velocity)*/
double halo_f(double rad, double vel)
{
  double E = 0.5 * vel * vel + halo_get_potential_from_radius(rad);
  double q = halo_E_to_q(E);

  return halo_f_q(q);
}

/*generate velocities for Hernquist distribution function with von Neumann rejection technique*/
double halo_generate_v(double rad)
{
  double pot = halo_get_potential_from_radius(rad);
  double v_max = sqrt(-2 * pot);	// escape velocity
  double v_guess, x_aux;
  double f_max = v_max * v_max * halo_f(rad, 0);

  v_guess = gsl_rng_uniform(random_generator) * v_max;
  x_aux = gsl_rng_uniform(random_generator) * f_max;

  while(x_aux > v_guess * v_guess * halo_f(rad, v_guess))
    {
      v_guess = gsl_rng_uniform(random_generator) * v_max;
      x_aux = gsl_rng_uniform(random_generator) * f_max;
    }
  return v_guess;
}

/* functions to calculate the inverse cumulative mass of halo profiles  - Alex 04/2020 */
double nfw_cm(double x, double r0, double q) {
  return (log(1 + x / r0) - x / (x + r0)) / (log(1+All.Halo_C) - All.Halo_C/(1+All.Halo_C)) - q;
}

double iso_cm(double x, double r0, double q) {
  return (x / r0 - atan(x / r0)) / (All.Halo_C - atan(All.Halo_C)) - q;
}

double burkert_cm(double x, double r0, double q) {
  return (log(1 + x / r0) + 0.5 * log(1 + (x * x) / (r0 * r0)) - atan(x / r0)) /
         (log(1+All.Halo_C) + 0.5*log(1+All.Halo_C*All.Halo_C) - atan(All.Halo_C)) - q;
}

double einasto_cm(double x, double r0, double q, double gamma) {
  return (gsl_sf_gamma(3/gamma) -
          gsl_sf_gamma_inc(3/gamma, 2 * pow(x / r0, gamma) / gamma))
          / gsl_sf_gamma(3/gamma) - q;
}

/* find the inverse cumulative mass value 'r' for a halo type */
double solve_secant(int htype, double q, double r0, double x0, double x1, double gamma, double err) {
  double i = 0, x2, error;

  switch (htype) {
    case 0: // Hernquist
      x2 = r0 * sqrt(q) / (1 - sqrt(q));
      break;
    case 1: // NFW
      do {
        x2 = (x0 * nfw_cm(x1, r0, q) - x1 * nfw_cm(x0, r0, q)) / (nfw_cm(x1, r0, q) - nfw_cm(x0, r0, q));
        i++;
        error = fabs(x2 - x1);
        x0 = x1;
        x1 = x2;
      } while (error > err && i < 100000);
      break;
    case 2: // Isothermal
      do {
        x2 = (x0 * iso_cm(x1, r0, q) - x1 * iso_cm(x0, r0, q)) / (iso_cm(x1, r0, q) - iso_cm(x0, r0, q));
        i++;
        error = fabs(x2 - x1);
        x0 = x1;
        x1 = x2;
      } while (error > err && i < 100000);
      break;
    case 3: // Burkert
      do {
        x2 = (x0 * burkert_cm(x1, r0, q) - x1 * burkert_cm(x0, r0, q)) / (burkert_cm(x1, r0, q) - burkert_cm(x0, r0, q));
        i++;
        error = fabs(x2 - x1);
        x0 = x1;
        x1 = x2;
      } while (error > err && i < 100000);
      break;
    case 4: // Einasto
      do {
        x2 = (x0 * einasto_cm(x1, r0, q, gamma) - x1 * einasto_cm(x0, r0, q, gamma)) /
             (einasto_cm(x1, r0, q, gamma) - einasto_cm(x0, r0, q, gamma));
        i++;
        error = fabs(x2 - x1);
        x0 = x1;
        x1 = x2;
      } while (error > err && i < 100000);
      break;
    default:
      printf("Wrong halo type %d (solve_secant).\n", htype);
      x2 = 0;
      break;
  }

  return x2;
}
/* Alex 04/2020 */
