#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_sf_gamma.h>

#include "allvars.h"
#include "proto.h"

/* struct for jdisk_int parameters - Alex 04/2020 */
struct my_f_params {double diskh;
                    double diskmass;
                   };

/* functions to calculate fc
 * Alex 04/2021
 */
static double hern_fc(double c) {
  return c * (0.5 - 0.5 / pow(1 + c, 2) - log(1 + c) / (1 + c)) / pow(log(1 + c) - c / (1 + c), 2);
}

static double iso_fc(double r, void *param) {
  return pow(1 / All.Halo_Rs - atan(r/All.Halo_Rs) / r, 2) / pow(All.Halo_C - atan(All.Halo_C), 2) * All.R200;
}

static double nfw_fc(double r, void *param) {
  return pow(1 / r * log(1 + r/All.Halo_Rs) - 1 / (r + All.Halo_Rs), 2) / 
         pow(log(1 + All.Halo_C) - All.Halo_C / (1 + All.Halo_C), 2) * All.R200;
}

static double burkert_fc(double r, void *param) {
  return pow(1 / r * (log(1 + r/All.Halo_Rs) + 0.5 * log(1 + (r*r)/(All.Halo_Rs*All.Halo_Rs)) - atan(r/All.Halo_Rs)), 2) / 
         pow(log(1 + All.Halo_C) + 0.5 * log(1 + All.Halo_C*All.Halo_C) - atan(r/All.Halo_Rs), 2) * All.R200;
}

static double einasto_fc(double r, void *param) {
  return pow(1 / r * (gsl_sf_gamma(3/All.GammaEin) - gsl_sf_gamma_inc(3/All.GammaEin, 2 / All.GammaEin * pow(r/All.Halo_Rs, All.GammaEin))), 2) /
         pow(gsl_sf_gamma(3/All.GammaEin) - gsl_sf_gamma_inc(3/All.GammaEin, 2 / All.GammaEin * pow(All.Halo_C, All.GammaEin)), 2) * All.R200;
}
/* Alex 04/2021 */

/* Commented - Alex 04/2021
static double fc(double c)
{
  return c * (0.5 - 0.5 / pow(1 + c, 2) - log(1 + c) / (1 + c)) / pow(log(1 + c) - c / (1 + c), 2);
}
*/
/* Renewed function to calculate fc depending on the Halo distribution
 * Alex 04/2021
 */
static double fc(double c, double R200, int htype) {
  /* variables for integration */
  gsl_function F;
  gsl_integration_workspace *workspace = gsl_integration_workspace_alloc(WORKSIZE);
  double result, abserr; /* store result and error */

  switch(htype) {
    case 0: /* Hernquist */
      result = hern_fc(c);
      break;
    case 1: /* NFW */
      F.function = &nfw_fc;
      gsl_integration_qag(&F, 0, R200, 0, 1.0e-8, WORKSIZE, GSL_INTEG_GAUSS41, workspace, &result, &abserr);
      gsl_integration_workspace_free(workspace);
      break;
    case 2: /* Pseudo-isothermal */
      F.function = &iso_fc;
      gsl_integration_qag(&F, 0, R200, 0, 1.0e-8, WORKSIZE, GSL_INTEG_GAUSS41, workspace, &result, &abserr);
      gsl_integration_workspace_free(workspace);
      break;
    case 3: /* Burkert */
      F.function = &burkert_fc;
      gsl_integration_qag(&F, 0, R200, 0, 1.0e-8, WORKSIZE, GSL_INTEG_GAUSS41, workspace, &result, &abserr);
      gsl_integration_workspace_free(workspace);
      break;
    case 4: /* Einasto */
      F.function = &einasto_fc;
      gsl_integration_qag(&F, 0, R200, 0, 1.0e-8, WORKSIZE, GSL_INTEG_GAUSS41, workspace, &result, &abserr);
      gsl_integration_workspace_free(workspace);
      break;
    default:
      printf("\nHalo distribution %d does not exist.", htype);
      exit(0);
      break;
  }
  return result;
}

static double jdisk_int(double x, void *param)
{
  double vc2, Sigma0, vc, y;
  struct my_f_params *params = (struct my_f_params *) param; // Alex 04/2020

  double diskh = (params -> diskh); // Alex 04/2020
  double diskmass = (params -> diskmass);

  if(x > 1.0e-10 * diskh)
    vc2 = All.G * (halo_get_mass_inside_radius(x) + bulge_get_mass_inside_radius(x)) / x;
  else
    vc2 = 0;

  if(vc2 < 0)
    terminate("vc2 < 0");

  Sigma0 = diskmass / (2 * M_PI * diskh * diskh);
  y = x / (2 * diskh);

  if(y > 1e-4)
    vc2 +=
      x * 2 * M_PI * All.G * Sigma0 * y * (gsl_sf_bessel_I0(y) * gsl_sf_bessel_K0(y) -
					   gsl_sf_bessel_I1(y) * gsl_sf_bessel_K1(y));

  vc = sqrt(vc2);

  return pow(x / diskh, 2) * vc * exp(-x / diskh);
}

static double gc_int(double x, void *param)
{
  return pow(log(1 + x) - x / (1 + x), 0.5) * pow(x, 1.5) / pow(1 + x, 2);
}

void structure_determination(void)
{
  double jhalo, jdisk, jd;
  double jdisk2; /* fraction of angular momentum for gas disk - Alex 04/2020 */
  double hnew, dh;

  /* total galaxy mass */
  All.M200 = pow(All.V200, 3) / (10 * All.G * All.Hubble);

  /* virial radius of galaxy */
  All.R200 = All.V200 / (10 * All.Hubble);

  All.LowerDispLimit = pow(0.01 * All.V200, 2);

  /* determine the masses of all components */
  All.Disk_Mass = All.MD * All.M200;
  All.Bulge_Mass = All.MB * All.M200;

  /* determine the mass of
   * new disc component
   *
   * Alex 02/2020 */
  All.Disk2_Mass = All.FGasDisk * All.Disk_Mass;

  All.BH_Mass = All.MBH * All.M200;

  if(All.MBH > 0)
    All.BH_N = 1;
  else
    All.BH_N = 0;

  //All.Halo_Mass = All.M200 - All.Disk_Mass - All.Bulge_Mass - All.BH_Mass;
  /* Alex 11/2019 */
  All.Halo_Mass = All.M200 - All.Disk_Mass - All.Disk2_Mass - All.Bulge_Mass - All.BH_Mass;

  /* modified - Alex 04/2020 */
  if (All.ShiftC) {
    /* set the scale factor of the halo */
    double hernm = mass_in_r200_estimate(0, All.Halo_Mass, All.Halo_C); /* Hernquist mass inside R200 */

    mpi_printf("\nMass of Hernquist profile at R200 = %g\n", hernm);

    /* calculate C for current halo profile */
    c_estimate(All.DistHalo, hernm, All.Halo_Mass, &All.Halo_C, 0.1, 0.001);
  }
  /* modified - Alex 04/2020 */

  //All.Halo_Rs = All.R200 / All.Halo_C; Alex 05/2020
  /* halo scale radius - Alex 05/2020*/
  if (!All.DistHalo || All.Halo_Rs <= 0)
    All.Halo_Rs = All.R200 / All.Halo_C;
  else if (All.DistHalo && All.Halo_Rs > 0)
    All.Halo_C = All.R200 / All.Halo_Rs;

  if (All.DistHalo)
    All.Halo_A = All.Halo_Rs;
  else
    All.Halo_A = All.Halo_Rs * sqrt(2 * (log(1 + All.Halo_C) - All.Halo_C / (1 + All.Halo_C)));
  /* modified - Alex 04/2020 */

  // Commented by Alex 04/2020
  /* set the scale factor of the hernquist halo */
  // All.Halo_A = All.Halo_Rs * sqrt(2 * (log(1 + All.Halo_C) - All.Halo_C / (1 + All.Halo_C)));

  jhalo = All.Lambda * sqrt(All.G) * pow(All.M200, 1.5) * sqrt(2 * All.R200 / fc(All.Halo_C, All.R200, All.DistHalo));
  jdisk = All.JD * jhalo;
  jdisk2 = All.JD * All.FJD2 * jhalo; // j_d of gas disk

  double halo_spinfactor =
    1.5 * All.Lambda * sqrt(2 * All.Halo_C / fc(All.Halo_C, All.R200, All.DistHalo)) * pow(log(1 + All.Halo_C) -
								   All.Halo_C / (1 + All.Halo_C),
								   1.5) / structure_gc(All.Halo_C);

  mpi_printf("\nStructural parameters:\n");
  mpi_printf("R200            = %g\n", All.R200);
  mpi_printf("M200            = %g  (this is the total mass)\n", All.M200);
  mpi_printf("CC              = %g\n", All.Halo_C);
  mpi_printf("A (halo)        = %g\n", All.Halo_A);
  mpi_printf("halo_spinfactor = %g\n", halo_spinfactor);

  /* first guess for disk scale length */
  All.Disk_H = sqrt(2.0) / 2.0 * All.Lambda / fc(All.Halo_C, All.R200, All.DistHalo) * All.R200;
  All.Disk_Z0 = All.DiskHeight * All.Disk_H;	/* sets disk thickness */

  All.Bulge_A = All.BulgeSize * All.Halo_A;	/* this will be used if no disk is present */

  /* Alex 11/2019 */
  MType[0] = All.Disk2_Mass;
  MType[1] = All.Halo_Mass;
  MType[2] = All.Disk_Mass;
  MType[3] = All.Bulge_Mass;

  /* Alex 11/2019 */
  NType[0] = All.Disk2_N;
  NType[1] = All.Halo_N;
  NType[2] = All.Disk_N;
  NType[3] = All.Bulge_N;

  if(All.Disk_Mass > 0)
  {
    do
    {
      jd = structure_disk_angmomentum(All.Disk_H, All.Disk_Mass);	/* computes disk momentum */

      hnew = jdisk / jd * All.Disk_H;

      dh = hnew - All.Disk_H;

      if(fabs(dh) > 0.5 * All.Disk_H)
        dh = 0.5 * All.Disk_H * dh / fabs(dh);
      else
        dh = dh * 0.1;

      All.Disk_H = All.Disk_H + dh;

      /* mpi_printf("Jd/J=%g   hnew: %g  \n", jd / jhalo, All.Disk_H);
      */

      All.Disk_Z0 = All.DiskHeight * All.Disk_H;	/* sets disk thickness */

    } while(fabs(dh) / All.Disk_H > 1e-5);
  }

  All.Disk2_H = All.Disk2_HF * All.Disk_H; /* scale length of disk2 */
  All.Disk2_Z0 = All.Disk2Height * All.Disk2_H;	/* sets disk2 thickness */

  /* Estimate gas disk scale length - Alex 04/2020 */
  if(All.Disk2_Mass > 0)
  {
    do
    {
      jd = structure_disk_angmomentum(All.Disk2_H, All.Disk2_Mass);	/* computes disk momentum */

      hnew = jdisk2 / jd * All.Disk2_H;

      dh = hnew - All.Disk2_H;

      if(fabs(dh) > 0.5 * All.Disk2_H)
        dh = 0.5 * All.Disk2_H * dh / fabs(dh);
      else
        dh = dh * 0.1;

      All.Disk2_H = All.Disk2_H + dh;

      /* mpi_printf("Jd/J=%g   hnew: %g  \n", jd / jhalo, All.Disk_H);
      */

      All.Disk2_Z0 = All.Disk2Height * All.Disk2_H;	/* sets disk thickness */

    } while(fabs(dh) / All.Disk2_H > 1e-5);
  }
  /* Alex 04/2020 */

  mpi_printf("H  (disk)       = %g\n", All.Disk_H);
  if (All.Disk2_Mass > 0) mpi_printf("H  (disk2)       = %g\n", All.Disk2_H); /* Alex 11/2019 */
  mpi_printf("Z0 (disk)       = %g\n", All.Disk_Z0);
  if (All.Disk2_Mass > 0) mpi_printf("Z0 (disk2)       = %g\n", All.Disk2_Z0); /* Alex 11/2019 */
  mpi_printf("A (bulge)       = %g\n", All.Bulge_A);
}


double structure_disk_angmomentum(double diskh, double diskmass)
{
  gsl_function F;
  gsl_integration_workspace *workspace = gsl_integration_workspace_alloc(WORKSIZE);
  struct my_f_params params = {diskh, diskmass};

  F.function = &jdisk_int;
  F.params = &params;

  double result, abserr;

  gsl_integration_qag(&F, 0, dmin(30 * diskh, All.R200),
		      0, 1.0e-8, WORKSIZE, GSL_INTEG_GAUSS41, workspace, &result, &abserr);

  result *= diskmass;

  gsl_integration_workspace_free(workspace);

  return result;
}

double structure_gc(double c)
{
  gsl_function F;
  gsl_integration_workspace *workspace = gsl_integration_workspace_alloc(WORKSIZE);
  F.function = &gc_int;

  double result, abserr;

  gsl_integration_qag(&F, 0, c, 0, 1.0e-8, WORKSIZE, GSL_INTEG_GAUSS41, workspace, &result, &abserr);

  gsl_integration_workspace_free(workspace);

  return result;
}

/* Calculate new C for the corresponding halo distribution.
 * This depends on the Hernquist mass within R200.
 */
void c_estimate(int htype, double hernm, double hmass, double *c, double ran, double incc) {
  double mass, lim = 0;
  int t = 50 / incc; /* number of iterations that C is computed */

  mass = mass_in_r200_estimate(htype, hmass, *c);

  while ((mass < (hernm - ran)) || (mass > (hernm + ran))) {
    lim++;
    if (lim > t) /* once the limit is reached, return current C */
      return;

    if (mass < hernm)
      *c += incc;
    else
      *c -= incc;

    mass = mass_in_r200_estimate(htype, hmass, *c);
  }
  return;
}

/* calculate mass inside r200 */
double mass_in_r200_estimate(int htype, double hmass, double c) {
  double mass;

  switch (htype) {
    case 0: // Hernquist
      mass = hmass * pow(c, 2) / pow(1 + c, 2);
      break;
    case 1: // NFW
      mass = hmass * (log(1 + c) - c / (1 + c));
      break;
    case 2: // Pseudo-isothermal
      mass = hmass * (c - atan(c));
      break;
    case 3: // Burkert
      mass = hmass * (log(1 + c) + 0.5 * log(1 + c * c) - atan(c));
      break;
    case 4: // Einasto
      mass = hmass * (gsl_sf_gamma(3/All.GammaEin) -
                     gsl_sf_gamma_inc(3/All.GammaEin, 2 * pow(c, All.GammaEin) / All.GammaEin))
                     / gsl_sf_gamma(3/All.GammaEin);
      break;
    default:
      printf("\nHalo distribution %d does not exist.", htype);
      exit(0);
      break;
  }
  return mass;
}
