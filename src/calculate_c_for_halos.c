#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define V200 160      /* virial velocity */
#define H    0.1      /* hubble constant */
#define G    43007.1  /* gravitational constant */

double c_estimate(int, double, double, double, double, double);
double mass_estimate(int, double, double);

int main() {
  double m200, hmass, hernm, cc, ran, incc, md;
  double ncc, nmass;
  int htype;

  htype = 3;
  md = 0.035;
  cc = 10.0;
  ran = 0.10;
  incc = 0.001;

  m200 = pow(V200, 3) / (10 * G * H);
  hmass = (1 - md) * m200;

  hernm = hmass * pow(cc / (1 + cc), 2);

  ncc = c_estimate(htype, hernm, hmass, cc, ran, incc);

  printf("\nM200 = %g\nHalo total mass = %g\nHernquist mass = %g\nC = %g\nNew C = %g\n",
          m200, hmass, hernm, cc, ncc);

  nmass = mass_estimate(htype, hmass, ncc);
  printf("\nMass for desired halo: %g\n", nmass);

  return 0;
}

double c_estimate(int htype, double hernm, double hmass, double cc, double ran, double incc) {
  double mass, c = cc, lim = 0;
  int t = 50 / incc;

  if (htype == 0)
    return c;

  mass = mass_estimate(htype, hmass, c);

  while ((mass < (hernm - ran)) || (mass > (hernm + ran))) {
    lim++;
    if (lim > t)
      return c;
    if (mass < hernm)
      c += incc;
    else
      c -= incc;

    mass = mass_estimate(htype, hmass, c);
  }
  return c;
}

double mass_estimate(int htype, double hmass, double c) {
  double mass;

  switch (htype) {
    case 1: // NFW
      mass = hmass * (log(1 + c) - c / (1 + c));
      break;
    case 2: // Pseudo-isothermal
      mass = hmass * (c - atan(c));
      break;
    case 3: // Burkert
      mass = hmass * (log(1 + c) + 0.5 * log(1 + c * c) - atan(c));
      break;
    default:
      printf("\nHalo distribution %d does not exist.", htype);
      exit(0);
      break;
  }
  return mass;
}
