#include <stdio.h>
#include <math.h>
#include <gsl/gsl_sf_gamma.h>

double solve_secant(int htype, double q, double r0, double x0, double x1, double gamma, double err);

double nfw_cm(double x, double r0, double q);
double iso_cm(double x, double r0, double q);
double burkert_cm(double x, double r0, double q);
double einasto_cm(double x, double r0, double q, double gamma);

double c = 10;
double r200 = 160;

int main() {
  double x0, x1, x2, error;
  int i = 0;

  double q1 = 0.9999999999;
  double q2 = 0.0000000001;
  int htype = 2;
  double gamma = 0.5;

  double r0;
  if (htype)
    r0 = r200 / c;
  else
    r0 = (r200 / c) * sqrt(2 * (log(1 + c) - c / (1 + c)));

  x0 = r0 * q2;
  //x0 = 100.0;
  x1 = x0 + 1;
  error = 0.00005;

  printf("\nr = %4.6g\n", solve_secant(htype, q1, r0, x0, x1, gamma, error));
  printf("\nr = %4.6g\n", solve_secant(htype, q2, r0, x0, x1, gamma, error));

  return 0;
}

double nfw_cm(double x, double r0, double q) {
  return log(1 + x / r0) - x / (x + r0) - q;
}

double iso_cm(double x, double r0, double q) {
  return x / r0 - atan(x / r0) - 5000 * q * (c - atan(c));
}

double burkert_cm(double x, double r0, double q) {
  return log(1 + x / r0) + 0.5 * log(1 + (x * x) / (r0 * r0)) - atan(x / r0) - q;
}

double einasto_cm(double x, double r0, double q, double gamma) {
  return (gsl_sf_gamma(3/gamma) -
          gsl_sf_gamma_inc(3/gamma, 2 * pow(x / r0, gamma) / gamma))
          / gsl_sf_gamma(3/gamma) - q;
}

/* find the inverse cumulative mass value 'r' for a halo type */
double solve_secant(int htype, double q, double r0, double x0, double x1, double gamma, double err) {
  double i = 0, x2, error;

  switch (htype) {
    case 0: // Hernquist
      x2 = r0 * (q + sqrt(q)) / (1 - q);
      break;
    case 1: // NFW
      do {
        x2 = (x0 * nfw_cm(x1, r0, q) - x1 * nfw_cm(x0, r0, q)) / (nfw_cm(x1, r0, q) - nfw_cm(x0, r0, q));
        i++;
        error = fabs(x2 - x1);
        x0 = x1;
        x1 = x2;
      } while (error > err && i < 100000);
      break;
    case 2: // Isothermal
      do {
        x2 = (x0 * iso_cm(x1, r0, q) - x1 * iso_cm(x0, r0, q)) / (iso_cm(x1, r0, q) - iso_cm(x0, r0, q));
        i++;
        error = fabs(x2 - x1);
        x0 = x1;
        x1 = x2;
      } while (error > err && i < 100000);
      break;
    case 3: // Burkert
      do {
        x2 = (x0 * burkert_cm(x1, r0, q) - x1 * burkert_cm(x0, r0, q)) / (burkert_cm(x1, r0, q) - burkert_cm(x0, r0, q));
        i++;
        error = fabs(x2 - x1);
        x0 = x1;
        x1 = x2;
      } while (error > err && i < 100000);
      break;
    case 4: // Einasto
      do {
        x2 = (x0 * einasto_cm(x1, r0, q, gamma) - x1 * einasto_cm(x0, r0, q, gamma)) /
             (einasto_cm(x1, r0, q, gamma) - einasto_cm(x0, r0, q, gamma));
        i++;
        error = fabs(x2 - x1);
        x0 = x1;
        x1 = x2;
      } while (error > err && i < 100000);
      break;
    default:
      printf("Wrong halo type %d (solve_secant).\n", htype);
      break;
  }

  return x2;
}
